import React, { useState } from "react";
import ReactDOM from "react-dom";

const appRoot = document.getElementById("root");
const modalRoot = document.getElementById("modal-root");

class Modal extends React.Component {
    constructor(props) {
        super(props);
        this.el = document.createElement("div");
    }

    componentDidMount() {
        modalRoot.appendChild(this.el);
    }

    render() {
        return ReactDOM.createPortal(this.props.children, this.el);
    }
}

const Parent = () => {
    // react hook
    const [clicks, setclicks] = useState(0);

    // zwracamy diva z kliknięciem oraz Modal z Child jako jego dzieckiem.
    return (
        <div onClick={() => setclicks(clicks + 1)}>
            <p>Number of clicks: {clicks}</p>
            <p>Open up th ebrowser</p>
            <Modal>
                <Child />
            </Modal>
        </div>
    );
};

function Child() {
    // zwracamy div z przyciskiem, nic więcej
    return (
        <div className="modal">
            <button>Click</button>
        </div>
    );
}

ReactDOM.render(<Parent />, appRoot);
